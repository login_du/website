var logIn = {
	init : function(){
		this.initDomUrl = "http://192.168.1.197:3000"; // 请求页面数据
		this.addEvent();
		 logIn.toolMethod.randomNum();
	},
	
	addEvent : function(){
		// 点击登录后表单验证  看输入是否为空或是否违法
        $(".acc_login").click(function() {
           var isTrue =  logIn.toolMethod.loginVerification(".con_login");  // 登陆验证  如果为空时的提示
           if(isTrue){
                var dataUrl = logIn.initDomUrl+"/service/login";
                var params = { 
                    MemberCode:$(".con_login .login_acc").val(),
                    MemberPw:$(".con_login .pwd").val(),
                };
                $.post(dataUrl,params,function(json){
                    logIn.analysisJson(json);
                });
            }
        });
        
        // y验证码输入框获取焦点事件
        $(".con_login .refresh_img").click(function(){
            // 生成随机数 显示在验证码栏
            $(".v_code_val").css({display:"inline-block"});
            logIn.toolMethod.randomNum();
        });
        // 点击验证码图片按钮 刷新图片
        $(".v_code_val").click(function(){
            logIn.toolMethod.randomNum();
        });
	},
	
	
	toolMethod : {
		// 生成随机数 显示在验证码栏
        randomNum : function(){
            var num = parseInt(Math.random()*9000+1000);
            $(".v_code_val").text(num);
        },
		
		// 登陆验证
        loginVerification : function(sel){
        	if($(".con_login .login_acc").val() == ""){
        		selNumError.init("请输入用户名!");
        		return false;
        	}else if($(".con_login .pwd").val() == ""){
        		selNumError.init("请输入密码!");
        		return false;
        	}else if($(".con_login .vCode").val() == ""){
        		selNumError.init("请输入验证码!");
        		return false;
        	}else if($(".con_login .vCode").val() != $(".con_login .v_code_val").text()){
            	selNumError.init("验证码输入错误!");
            	return false;
            }
            return true;
        },
	},
	
	
	analysisJson : function(data){
//      Index.AccountInfo = data.ActionCode;// 将数据存储来等使用
        if(data.ErrMsg == "00000000"){  // 表示数据正确  登陆成功
        	data = JSON.stringify(data);
			document.cookie = data;
        	window.location.href = "index.html";
        }else{
            alert("登录失败");
        }
    },
};
logIn.init();
