var BannerS = {
	init: function() {
		// 获取浏览器宽度
		BannerS.bodyWidth = $("body").css("width");
		// 设置图片宽度
		$(".banner_l img").css("width", BannerS.bodyWidth);

		BannerS.setImgLeft();
		setTimeout("BannerS.play()",4000);

		BannerS.addEvent();
	},

	play: function() {
		var dataId = $(".banner_l img:first-child").attr("data-id");
		$(".ba_list .ba_item").removeClass("active");
		$($(".ba_list .ba_item")[dataId]).addClass("active")
		// 第一张图片移动后  将其添加到最后
		$(".banner_l img:first-child").animate({
			left: "-=" + BannerS.bodyWidth
		}, 1000, function() {
			$(".banner_l").append($(".banner_l img:first-child"));
			
		});
		$(".banner_l img:nth-child(2)").animate({
			left: 0
		}, 1000);
		BannerS.setImgLeft();
		BannerS.time = setTimeout("BannerS.play()", 4000);
	},

	setImgLeft: function() {
		$(".banner_l img:nth-child(2)").css("left", BannerS.bodyWidth);
		$(".banner_l img:nth-child(3)").css("left", BannerS.bodyWidth);
	},

	addEvent: function() {
		$(".ba_list .ba_item").click(function() {
			clearTimeout(BannerS.time);
			var index = $(this).index($(".ba_list .ba_item"));
			var indexActive = $(".ba_list .active").index($(".ba_list .ba_item"));
			$(".ba_list .ba_item").removeClass("active");
			$(this).addClass("active")
			if(index == indexActive) {} else {
				$(".banner_l img:nth-child(" + index + ")").animate({
					left: "-=" + BannerS.bodyWidth
				}, 1000, function() {
					$(".ba_list .ba_item").each(function(){
						if($(this).hasClass(".active")){
						}else{
							$(".banner_l").append($("this"));
						}
					});
					BannerS.time = setTimeout("BannerS.play()", 4000);// 开启计时器
				});
			}

		});
	},
};
var Home = {
	init: function() {
		BannerS.init();
	}
};
Home.init();